# Table of Contents

1. [Day 1](#day-1)
   - [Intro 1](#intro-1)
   - [What is Engineering](#what-is-engineering)
   - [Types of Engineering](#types-of-engineering)
   - [Activity: Spaghetti Tower Challenge](#activity-spaghetti-tower-challenge)
   - [Engineering Design Process Overview](#engineering-design-process-overview)
   - [Conclusion 1](#conclusion-1)
2. [Day 2](#day-2)
   - [Intro 2](#intro-2)
   - [Understand the Problem](#understand-the-problem)
   - [Complete background research](#complete-background-research)
   - [Mini activity: Seach engine challenge](#mini-activity-seach-engine-challenge)
   - [Imagine and evaluate possible solutions](#imagine-and-evaluate-possible-solutions)
   - [Develop and prototype solution](#develop-and-prototype-solution)
   - [Activity: Pet Home 1](#activity-pet-home-1)
   - [Conclusion 2](#conclusion-2)
3. [Day 3](#day-3)
   - [Intro 3](#intro-3)
   - [Test Solution](#test-solution)
   - [Activity: Test the Micro:Pet Home](#activity-test-the-micropet-home)
   - [Assess prototype and reiterate](#assess-prototype-and-reiterate)
   - [Activity: Pet Home 2](#activity-pet-home-2)
   - [Conclusion 3](#conclusion-3)
4. [Day 4](#day-4)
   - [Intro 4](#intro-4)
    - [Communicate Results](#communicate-results)
    - [Activity: Building on a Budget](#activity-building-on-a-budget)
    - [Conclusion 4](#conclusion-4)
5. [Day 5](#day-5)
   - [Intro 5](#intro-5)
   - [Activity: Spaghetti Tower Challenge 2.0](#activity-spaghetti-tower-challenge-20)
   - [Conclusion 5](#conclusion-5)


# Day 1
## Intro 1
Welcome to Day 1 of Engineering with TACoS! This video introduces today's topics and shows what you'll need to get started the activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day1
## What is Engineering
This video defines engineering and provides examples of engineering in everyday life.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day1
## Types of Engineering
This video introduces the different types of engineering and provides examples of each.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day1
## Activity: Spaghetti Tower Challenge
This video introduces the Spaghetti Tower Challenge. This activity will give you a chance to think like a civil engineer by designing and constructing a building.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day1
## Engineering Design Process Overview
This video introduces the Engineering Design Process, the process used by engineers to solve problems.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day1
## Conclusion 1
This video shows a summary of all the activities and lessons completed in Day 1. Great job on your first day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day1
# Day 2
## Intro
Welcome to Day 2 of Engineering with TACoS! This video introduces today's topics and shows what you'll need to get started the activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Understand the Problem
This video introduces the first step of the Engineering Design Process: Understand the Problem. This step involves defining the problem and identifying the criteria and constraints.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Complete background research
This video introduces the second step of the Engineering Design Process: Complete background research. This step involves researching the problem and possible solutions.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Mini activity: Seach engine challenge
This video introduces the Search Engine Challenge. This activity will give you a chance to put your search engine skills to the test and practice researching!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Imagine and evaluate possible solutions
This video introduces the third step of the Engineering Design Process: Imagine and evaluate possible solutions. This step involves brainstorming and evaluating possible solutions.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Develop and prototype solution
This video introduces the fourth step of the Engineering Design Process: Develop and prototype solution. This step involves actually building a prototype of the solution.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Activity: Pet Home 1
This video introduces the Pet Home 1 activity. By the end of this video, you will have your first prototype of a Micro:pet home!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2
## Conclusion 2
This video shows a summary of all the activities and lessons completed in Day 2. Great job on your second day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day2

Science Buddies website: https://www.sciencebuddies.org/science-fair-projects/engineering-design-process/engineering-design-process-steps
# Day 3
## Intro 3
Welcome to Day 3 of Engineering with TACoS! This video introduces today's topics and shows what you'll need to get started the activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day3
## Test Solution
This video introduces the fifth step of the Engineering Design Process: Test Solution. This step involves testing the prototype and making sure it works.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day3
## Activity: Test the Micro:Pet Home
This video introduces the Test the Micro:Pet Home activity. By the end of this video, you will have tested your Micro:pet home by using varying sound levels and recording noise data!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day3

Noise logger code: https://makecode.microbit.org/v4/_FMAdeoWhqavm

TACoS Computer Science, Day 1: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1#step-four---flashing-the-microbit
## Assess prototype and reiterate
This video introduces the sixth step of the Engineering Design Process: Assess prototype and reiterate. This step involves evaluating the prototype and making improvements.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day3
## Activity: Pet Home 2
This video introduces the Pet Home 2 activity. By the end of this video, you will have a final prototype of a Micro:pet home!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day3
## Conclusion 3
This video shows a summary of all the activities and lessons completed in Day 3. Great job on your third day!
# Day 4
## Intro 4
Welcome to Day 4 of Engineering with TACoS! This video introduces today's topics and shows what you'll need to get started the activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day4
## Communicate Results
This video introduces the seventh step of the Engineering Design Process: Communicate Results. This step involves sharing the results of the project with others.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day4
## Activity: Building on a Budget
This video introduces the Building on a Budget activity. By the end of this video, you will have a final prototype of your Micro:Pet that you built on a budget!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day4

Noise logger code: https://makecode.microbit.org/v4/_FMAdeoWhqavm
## Conclusion 4
This video shows a summary of all the activities and lessons completed in Day 4. Great job on your fourth day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day4
# Day 5
## Intro 5
Welcome to Day 5 of Engineering with TACoS! This video introduces today's topics and shows what you'll need to get started the activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day5
## Activity: Spaghetti Tower Challenge 2.0
This video introduces the Spaghetti Tower Challenge 2.0 activity. By the end of this video, you will have a final prototype of your Spaghetti Tower!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day5
## Conclusion 5
This video shows a summary of all the activities and lessons completed in Day 5. Great job on your fifth day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day5


