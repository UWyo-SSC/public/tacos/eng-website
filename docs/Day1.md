---
id: 'day1'
title: 'Day 1: Introduction to Engineering'
---
import useBaseUrl from '@docusaurus/useBaseUrl';

<iframe width="560" height="315" src="https://www.youtube.com/embed/pvBu-RuD6GE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to day 1 of The Artful Craft of Science - Engineering! Today we are going to begin our engineering journey. We'll start with an introduction to engineering and the types of engineering, then we'll jump right in and build the tallest possible tower out of spaghetti and marshmallows! At the end of the day, we'll talk a little about what the rest of this week will look like. Let's get started!

## What is Engineering?

<iframe width="560" height="315" src="https://www.youtube.com/embed/DH0BKSHIoRY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

What does the word "Engineering" make you think of? Do you think of car engines, wind turbines, and airplanes? Or do you think of skyscrapers, bridges, and roads? Maybe you think of computers and technology, MRI machines and pace makers, shoes and toothbrushes, or even oil refineries and nuclear energy. There are all sorts of things that you might think of because engineering is everywhere! 

Engineering is defined as the branch of science and technology concerned with the development and modification of engines, machines, structures, or other complicated systems and processes using specialized knowledge or skills. It is more than the application of science and math, it is an opportunity for creativity and exploration. Engineering is all about innovation and improves everything from human lives to buildings to music and more.

## Types of Engineering

<iframe width="560" height="315" src="https://www.youtube.com/embed/_WWNAMwDZC0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Engineering is a vast field, and there are many different types of engineers that work in different professions. This allows them to focus on specialized skills and gain deeper knowledge in their area of expertise. 

Can you guess what all of these engineers might do?

<details>
  <summary>Aerospace Engineers</summary>

> This field contributes to innovation in aviation, defense, and spacecraft. 

</details>

<details>
  <summary>Biomedical Engineers</summary>

> This field is dedicated to advancing medicine and medical technologies

</details>

<details>
  <summary>Civil Engineers</summary>

> These engineers design infrastructure like buildings, bridges, water systems and more 

</details>

<details>
  <summary>Environmental Engineers</summary>

> This field tackles challenges such as population growth and climate change, working to make sure humans and the environment thrive.

</details>

<details>
  <summary>Chemical & Biological Engineers</summary>

> These engineers develop new products in a variety of areas including pharmaceuticals, food, and materials.

</details>

<details>
  <summary>Mechanical Engineers</summary>

> This field focuses on the development and design of physical machines such as engines or turbines.

</details>

<details>
  <summary>Agricultural Engineers</summary>

> These engineers integrate technology into food growing and processing and help design farm equipment. 

</details>

<details>
  <summary>Electrical Engineers</summary>

> This field contributes to energy systems, develops electronics, and often works alongside other engineering disciplines to create products.

</details>

<details>
  <summary>Software Engineers</summary>

> These engineers design computer systems, program games and apps, and create new innovative technology.

</details>

<!-- ![Edit firefly code](/pictures/day1/editfirefly.gif) -->
<!-- TODO why aren't pictures working? -->

<!-- - Aerospace Engineers - this field contributes to innovation in aviation, defense, and spacecraft. 
- Biomedical Engineers - this field is dedicated to advancing medicine and medical technologies.
- Civil Engineers - these engineers design infrastructure like buildings, bridges, water systems and more.
- Environmental Engineers - this field tackles challenges such as population growth and climate change, working to make sure humans and the environment thrive.
- Chemical & Biological Engineers - these engineers develop new products in a variety of areas including pharmaceuticals, food, and materials.
- Mechanical Engineers - this field focuses on the development and design of physical machines such as engines or turbines.
- Agricultural Engineers - these engineers integrate technology into food growing and processing and help design farm equipment. 
- Electrical Engineers - this field contributes to energy systems, develops electronics, and often works alongside other engineering disciplines to create products.
- Software Engineers - these engineers design computer systems, program games and apps, and create new innovative technology. -->

This week we will be tackling challenges that an engineer might face. Let's start by stepping into the shoes of a civil engineer!

## Activity: Spaghetti Tower Challenge

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ez6P9xNq8UM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

For our first engineering adventure, we are going to think like a civil engineer to design and construct a building. We can't build entire skyscrapers, but we *can* build a tower out of spaghetti! Here's the engineering problem we want to solve: 

**Using limited materials how can we build the tallest possible spaghetti tower?**

But that's not all - there's a catch. Actually, there are two catches. First, your spaghetti tower must be able to support the weight of a big marshmallow at the top. Second, you only have 15 minutes to design and build your masterpiece. Let's get started!

### Step One - Gather materials

For this project, you can use:
- 20 Dry spaghetti noodles
- 1 Big marshmallow
- 20 Mini marshmallows
- A handful of rubber bands
- A ruler

Collect these items and make sure you have a nice flat surface to work on.

If this is your first time building a spaghetti tower, you are ready to move on to step two! If you've already done an activity like this before, or you feel particularly confident, consider taking on an extra challenge...

#### The surprise third catch

Are you already a spaghetti tower master? If that's the case, let's make this a bit more interesting! Go ahead and bundle up your spaghetti noodles, hold the bunch in both hands...

... and break them in HALF! Then, take 5 of your mini marshmallows and EAT THEM! (Or politely set them aside.) Now, you should have 40 *short* spaghetti noodles and only 15 marshmallows to work with.

### Step Two - Get set...

Remember, your goal here is to build a spaghetti tower that is as **tall as possible** that can hold an **entire big marshmallow on top**. You may want to plan ahead, but don't forget that you have a limited amount of time! We will give you fifteen minutes to plan, build, adapt, and test your tower. 

Before you get started, set your big marshmallow aside -- although, you may want to keep it close by for testing purposes -- you will officially attempt to place this marshmallow on top of your tower **after** the building time is up. Make sure you think ahead and have a plan for how you will attach this marshmallow. The spaghetti may be too fragile to force it on at the very end. 

Are you ready? Let's put fifteen minutes on the clock! (Or, if you're online, you can follow this link to a fifteen-minute timer video). Ready... set, go!

### Step Three - Go!

<iframe width="560" height="315" src="https://www.youtube.com/embed/bpWbZri64bI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Step Four - Test your tower
#### Step 4a: Place your mashmallows
Welcome back! At this point, your fifteen minute timer should be up and you should have a tall tower in front of you that is prepared to take on the marshmallow test. When you're ready, go ahead and carefully place your marshmallow atop your tower... Good luck!

#### Step 4b: Reflection
How did it go? Did your tower stand tall or did it topple under the weight of the sugary snack? If your tower managed to survive, congratulations! If not, don't worry because making mistakes is an important part of the engineering process. In fact, a failed test can be even more informative than a successful one. 

Regardless of your tower's apparent success, can you think of a way that you could have made it better? Keep this question in the back of your mind because you'll get another shot at the spaghetti tower challenge at the end of the week!

## Engineering design process overview

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q1L_M6rUMU8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Over the next few days, we're going to talk all about how engineers approach problems just like the spaghetti tower challenge. No matter their discipline, engineers use a design process that helps them create something new or improve something old. This design process is made up of a series of steps:
1. Understand the problem.
2. Complete background research.
3. Imagine and evaluate possible solutions.
4. Develop and prototype a solution.
5. Test solution.
6. Assess prototype and reiterate.
7. Communicate results.

One special thing to note about the engineering design process is that it is iterative, meaning it repeats over and over, and may jump back and forth between a few steps before moving on. This lets engineers make continuous improvements and helps them learn. Open-ended design is a focus of this process, so be as creative as you can be within the limits of your materials and your imagination. It also requires teamwork, so be sure to hear out everyone you work with – someone might have a solution you hadn’t thought about yet!

We'll learn more about each step of the engineering design process throughout the week. In the meantime, if you're curious about how the engineering design process is like a taco party? Check out this video!

<iframe width="560" height="315" src="https://www.youtube.com/embed/MAhpfFt_mWM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/tTx0bRLD2dA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Great job today, we covered a lot! We learned that engineering is a vast field. From skyscrapers to microchips, engineering is found everywhere in our lives, and engineers are constantly working to improve the world around us. We talked about the types of engineering and we tried our hand at being civil engineers by building weight-bearing spaghetti towers. We finished the day by introducing the engineering design process, which you will use this week to make your very own designs! Tomorrow, we'll introduce a new engineering problem and talk more in depth about how to plan and prototype the best solutions! 
