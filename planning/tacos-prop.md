# TACoS Curriculum Proposal

## Fireflies

### Prompt

Swarms of fireflies manage to sync up their flashing without anybody telling them to do so! In this lesson we will teach microbits to do the same thing.

### Objectives

- Understand how fireflies sync up
- Understand how Micro:Bits communicate
- Understand how to put basic programs on Micro:Bits

### Lesson

1. Introduce how to put simple programs on the Micro:Bit
2. Send messages between Micro:Bits
3. Show off how Fireflies sync up
4. Present basic Micro:Bit algorithm
5. Program synchronization of Micro:Bits

## Bees

### Prompt

Bees take on multiple jobs in the process of making honey. In this lesson, we'll program three different bees: a pollinator bee who gathers nectar; a worker bee who processes the nectar into honey; and a storage bee who takes the honey, puts it in the comb and seals it up with bees wax.

### Objectives

- Understand the process of making honey
- Understand the ways to interface with the Micro:Bits
- Understand how to send more complex information between Micro:Bits

### Lesson

1. Introduce the ways to interact with the Micro:Bits (i.e. buttons, shaking, compass, etc.)
2. Show off how Bees make honey
3. Program a pollinator bee to "go collect pollen"
4. Program a worker bee to "process the honey"
5. Program a storage bee to track how much honey has been made

## Bears

### Prompt

Bears hibernate throughout the winter as a way to conserve energy. In this lesson, we'll learn how to use sensors to understand how close winter is and program a bear to know if it should be hibernating, just drinking water, or scrounging for food.

### Objectives

- Understand the hibernation cycle
- Understand how instruments and sensors connect to Micro:Bits
- Understand control flow and decision making in programs

### Lesson

1. Introduce If...Then programming structures
2. Introduce sensors
3. Show off how Bears hibernate
4. Write a program to take temperature and decide if the bear should be eating, drinking, or sleeping

## Monarch Butterflies

Introduce swarm behaviors
Extend sensing to plan migration
Extend communication to plan migration

## Orioles

Introduce predators
Have a way to "eat" the monarchs
Challenge students to try to find a way to protect the butterflies
