# How to edit these files in makecode 
As of now, this is the best way to update our microbit code
- Download the file 
- Go to https://makecode.microbit.org/ 
- Click "Import" on top, right-hand side 
- Select "Import File" and locate the downloaded file 
- Make your edits 
- Upload the .hex file from makecode 
- Replace in github 

# microbit-bumblebee-controller.hex
Flash to micro:bit conntected to controller. Bumblebee starts as a dim LED that moves quickly (delay set to 55 on start). As the bee collects pollen, it becomes brighter and moves slower as if it were heavier. On shake, the bumblebee delivers its pollen to the hive by communicating with microbit-hive microbit on radiogroup 12. 

# microbit-hive.hex
Flash to free standing micro:bit. Recieves messages from microbit-bumblebee-controller (radiogroup 12) that fill its honey pot. Every 10 pollen collected = 1 honey. 

# microbit-fireflies.hex
Flash to all three micro:bits and watch them sync! 

# microbit-spider.hex
Modify the hive to be on a separate channel from the bees and intercept transmissions from the bees to the hive.