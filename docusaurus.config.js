module.exports = {
  title: 'TACoS 2023: Engineering',
  tagline: '',
  url: 'https://community.pages.uwcedar.io/',
  baseUrl: '/wycs/tacos/2023/eng-website/',
  favicon: 'img/seed.png',
  organizationName: 'WyCS', // Usually your GitHub org/user name.
  projectName: 'website', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'TACoS 2023: Engineering',
      logo: {
        alt: 'My Site Logo',
        src: 'img/seed.png',
      },
      items: [
        {
          href: 'https://uwcedar.io/community/wycs/tacos/2023/eng-website',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Connect',
          items: [
            {
              label: 'Email',
              href: 'mailto:andrea.burrows@uwyo.edu,mike.borowczak@uwyo.edu',
            },
            {
              label: 'Facebook',
              href: 'https://www.facebook.com/uwcedar',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/WyCS307',
            },
          ],
        },
        {
          title: 'Learn More',
          items: [
            {
              label: 'WyCS',
              href: 'https://www.uwyo.edu/wycs/',
            },
            {
              label: 'CEDAR Lab',
              href: 'https://uwcedar.github.io/digiflyer#slide=1',
            },
            {
              label: 'GenCyber',
              href: 'https://www.cs.uwyo.edu/~mborowcz/camps/cowpokes/',
            },
          ],
        },
        {
          title: 'Makecode Resources',
          items: [
            {
              label: 'Blocks Documentation',
              href: 'https://makecode.microbit.org/blocks',
            },
            {
              label: 'Makecode FAQ',
              href: 'https://makecode.microbit.org/faq',
            },
            {
              label: 'Makecode Docs',
              href: 'https://makecode.microbit.org/docs',
            },
          ],
        },
        {
          title: 'More Micro:bit Material',
          items: [
            {
              label: 'Intro CS w/ Micro:bit',
              href: 'https://makecode.microbit.org/v0/--docs#book:/courses/csintro/SUMMARY',
            },
            {
              label: 'Python User Guide',
              href: 'https://microbit.org/get-started/user-guide/python/',
            },
            {
              label: 'Javscript Tutorial',
              href: 'https://makecode.microbit.org/courses/blocks-to-javascript/hello-javascript',
            },
          ],
        },
      ],
      copyright: `Website built by the University of Wyoming CEDAR Lab with Docusaurus. Funding provided by MilliporeSigma and NSA GenCyber program.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://uwcedar.io/community/wycs/tacos/2023/eng-website',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://uwcedar.io/community/wycs/tacos/2023/eng-website',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
