---
title: "TACoS 2023: Engineering"
hide_table_of_contents: true
---


## Agenda
### [Day 1](Day1.md) - Intro to Engineering<br />
### [Day 2](Day2.md) - Plan and Build<br />
### [Day 3](Day3.md) - Test and Reiterate<br />
### [Day 4](Day4.md) - Communicate Results<br />
### [Day 5](Day5.md) - Bringing it All Together<br />


