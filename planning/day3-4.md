## Design Process Step 6: Test Solution
Congratulations! You have spent ample time creating and refining your final design. You have 

- Does it meet basic requirements
- How else might someone use this
- Put it to the test
  - the more users (testers) the better!
  - observe the tester:
    - what did they do
    - what went well
    - what did not go well
    - did you hit measureable targets

## Design Process Step 7: assess prototype and reiterate
- Two main points to address in the redesign process
  - Fix problems
  - Improve on the things that went well
- Main Questions:
  1. Is your user able to overcome the problem by using or interacting with your solution? 
    - If yes: what went well, how can you make it even better!
    - If no: focus on what hindered your success
  2. Does the user ever need to ask you any questions when using or interacting with your solution? 
    - If yes: where was the confusion, how can you make the functionality more obvious
  3. Does the user interact with your solution exactly the way that you intended for them to? 
    - If no: focus on unintended complications
  4. If you have measurable targets for your solution, did you meet them?
    - How can you make this better, cheaper, stronger, etc
(Ask students to figure out at least one thing that did not go well and one thing that did)
- Application Step:
    - Back to step 5
      - remind students what this was
      - help students apply what they learned to redesign


## Design Process Step 8: Communicate Results
- Create our own **small** write up document
  - Title page.
  - Abstract. An abstract is an abbreviated version of your final report.
  - Question, variables, and hypothesis.
  - Background research. This is the Research paper you wrote before you started your experiment.
  - Materials list.
  - Data analysis and discussion. This section is a summary of what you found out in your experiment, focusing on your observations, data table, and graph(s), which should be included at this location in the report.
  - Conclusions.
    - Ideas for future research. What advise would you give someone else