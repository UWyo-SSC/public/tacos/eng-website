---
id: 'day3'
title: 'Day 3: Test and Reiterate'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/HGIOIiO_X-A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to day 3 of The Artful Craft of Science - Engineering! Today we are going to go over steps 5 and 6 of the engineering design process:

- Step 5: Test solution
- Step 6: Assess prototype and reiterate

Now that our micro:pet homes are built, we need to test them out! 

## Test Solution

<iframe width="560" height="315" src="https://www.youtube.com/embed/H1CifJzef7E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Congratulations! You have spent ample time creating and refining your design. Now it is time to test it! During the test, we carefully watch and record what happens. You should pay attention to things like how well it works, if anything goes wrong, or if it needs any improvements. If our design didn't work as we hoped, we go back to the drawing board, brainstorm new ideas, and make improvements to our design based on what we learned from the test.

Testing is an important step because it helps us learn from our mistakes and make our designs better. It's like a way of experimenting and discovering what works and what doesn't. By testing our designs, we can make sure they are safe, effective, and solve the problems they were meant to solve.

Remember, testing is all about trial and error. It's okay if our design doesn't work perfectly the first time. That's part of the process, and it helps us become better engineers and problem solvers!

## Activity: Test the Micro:pet Home

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hh2k0Nttg8s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that we've built our micro:pet homes, we need to see how well they work! We'll use a micro:bit and the noise sensor to conduct a series of tests and collect data. Later, we'll use that data to improve our designs!

### Setup
For this activity, you will need: 
- A quiet space for testing
- Your micro:pet home, first prototype
- A computer, with Chrome installed
- Sensor:bit
- A micro:bit (plugged in to sensor:bit)
- A micro:bit cable
- Noise sensor (plugged in to pin 1)
- [Noise logger code](https://makecode.microbit.org/v4/_FMAdeoWhqavm)

### Step 1 - Prepare testing materials
#### Step 1a: Assemble hardware
Let's make sure our hardware is set up correctly. Your micro:bit should be plugged into the sensor:bit, and the noise sensor should be plugged into pin 1. 

![noise logger setup](/pictures/day3/noiselogger-setup.jpg)

#### Step 1b: Flash code
Start by flashing the [noise logger code](https://makecode.microbit.org/v4/_FMAdeoWhqavm) that we wrote in the computer science section onto a micro:bit. 

1. Plug your micro:bit, sensor attached, to your computer
2. Using Chorme, follow the link to the noise logger code
3. Click "edit" in the top right-hand part of the screen

![edit noise logger](/pictures/day3/editnoiselogger.png)

4. Connect your micro:bit to the Chrome Browser
5. One-click flash the code onto your micro:bit

If you're not sure how to connect your micro:bit to chrome and flash the code, follow the instructions in [TACoS Computer Science, Day 1](https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1#step-four---flashing-the-microbit).

#### Step 1c: Check connections
In makecode, there should be a button that appears below the virtual micro:bit that says "show console device". Click on that and make sure the graph is working. It should look something like this: 

![graph](/pictures/day3/graph.png)

### Step 2: Control
When doing an experiement, it is important to establish a "control." A control is a comparison group that you use to make sure your results are accurate. Before we start adding additional noise, it is important to know how loud your testing environment is. We will chart two "no noise" control groups. First, place the micro:bit **inside the box**. Run the sound detecting code for a few seconds. Next, place the mico:bit **outside the box** and run the code again. Be sure to store your sound graphs for later analysis!

### Step 3: Test
Now that we have established a baseline for our environment, we can start adding additional noise to test our Micro:pet Homes! This time, place your micro:bit **inside the box** and place a noise making device (phone, laptop) **outside the box**. Start with your volume at half the maximum loudness and watch as your micro:bit charts the sound! Next, place your micro:bit **outside the box** and place the noise making device **inside the box**. Play the noise at the same volume level (half the maximum) and record the sound levels.

Awesome! You have officially tested your Micro:pet Home. Great work! However, we only had one test case, half-max volume. Can you think of additional tests we could run? Here are a few suggestions:

- Quieter volume
- Louder volume
- High pitched sounds
- Low pitched sounds
- Using a phone notification as the sound (ringtone or textone)
- Playing typical outdoor noises:
  - traffic, construction, birds, crickets, etc

Pick 1-2 more test cases and repeat step 3. Remember to test the noise on the inside and outside of the box.

## Assess prototype and reiterate

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yq5a_TJUCuQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The "reiterate" step in the engineering design process involves going back and making improvements to our design based on what we learned during testing. It's like giving our design a second chance to be even better! After we test our design and analyze the results, we may find that some things didn't work as well as we wanted or that there are ways to make it even better.

1. **Gather** feedback and information from testing  
  i. Did your prototype meet the requirements?  
  ii. Was there a problem with your design?  
  iii. Can you improve your prototype to perform even better?  
2. **Brainstorm** new ideas or modify certain parts of your design
3. **Implement** your new ideas by building on your original prototype
4. **Repeat** the process. Test the new design to see if the changes we made helped to solve the problems or improve its performance.

The reiterate step is important because it allows us to refine and optimize our design. It helps us learn from our mistakes and make adjustments based on what we discovered during testing. By going through this cycle of testing and reiteration, we can make our designs stronger, more efficient, and better suited to solve the problems we're trying to tackle.

Remember, engineers don't give up when things don't work perfectly the first time. They keep trying, learning, and improving until they find the best solution. The reiterate step is all about that continuous improvement process, where each iteration brings us closer to a successful design.

## Activity: The Micro:Pet Home - Second Prototype

<iframe width="560" height="315" src="https://www.youtube.com/embed/vp6YgHAK1lY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that we've tested our prototype, it's time to return to the drawing board so that we can make improvements. We'll take some time to think about any problems that came up, and then we'll make a plan for improvements. After we've got a plan, we'll spend the rest of today building the second prototype of our micor:pet homes!

### Setup
For this project, you will need: 
- The plans for your first Micro:pet home prototype
- The box that your TACoS materials were provided in
- 50 cotton balls
- 2 feet of bubble wrap
- 2 felt sheets
- 2 foam sheets
- A handful of rubber bands

You should also grab:
- Scissors
- A roll of tape
- A ruler
- A writing utensil

If you brought additional materials (like another box, packing peanuts, cork board, tin foil, etc...) get those out, too!

### Step 1 - Identify and Fix Problems
First, we need to **fix problems** in our design. Think back to the list of requirments: 
- Reduce noise
- Use a limited number of materials
- Ensure your design fits inside the box
- Don't destroy the box
- Create a hole for the micro:bit cable

Did your prototype meet these requirements? Where there any obvious problems? If there were, these issues need to be addressed first.

Take some time to reflect on your design and think of how you can solve any big problems that came up. Once you have a game plan to make sure these requirements are met, you can move onto the second part.

### Step 2 - Improve Your Design
When working on the improvement phase, focus on the results from testing your Micro:pet Home. What performed well and what did not? How can you improve these areas?
  - Pick one thing that went well and improve this area. For example, if you found a material that was especially excellent at reducing sound, add more!
  - Next, pick one thing that did not go well. For example, if you were unable to cover the entire surface area of the box, consider redistrubtuing your materials for full coverage.

Once you've decided on some things to improve, take some time to draw out the plans for your second prototype. When you're detailed plans are complete, move on to step three!

### Step 3 - Apply New Knowledge
Now that you've reflected on your first prototype, you know what problems need to be fixed and you've got some ideas on how you can improve your design. It's time to build your upgraded micro:pet home! *While you're building, keep in mind that you'll still need to use your materials for your third prototype, so be careful!*

You've got the rest of the hour to implement your changes. Good luck!

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/DoUapepYTEU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Today, we learned more about the engineering design process. Specifically we learned about:

    Step 5: Test solution
    Step 6: Assess prototype and reiterate

We gained experience performing tests on our Micro:pet Home. To do this, we used varying noises at varying levels and recorded the sound measuremets from inside and outside the Micro:pet Home. Then, we analyzed the results of our tests. Finally, we used this information to reiterate our prototype design to produce an even better Micro:pet Home. Tomorrow, we will learn about how to document and communicate your specific design process.
