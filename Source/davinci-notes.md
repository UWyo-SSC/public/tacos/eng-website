## DaVinci Notes for TACoS
We'll use DaVinci to edit TACoS videos. DaVinci Resolve is free and beginner friendly. Go ahead and [download the lastest version here](https://www.blackmagicdesign.com/products/davinciresolve). I downloaded DaVinci Resolve v18. 

### Tutorials
To get started, Watch the tutorials on this page (part 1 and part 2): https://www.blackmagicdesign.com/products/davinciresolve/training

Feel free to speed it up a bit and skip through parts that aren't necessary. I've included my notes below, but you may want to take your own notes.

### DaVinci Media Storage
To make adding files to projects faster and easier, I suggest adding the folder where your TACoS files will be saved to your media storage in DaVinci. To do this, open preferences -> media storage, then click "add". Locate the folder and click "open".

### Editing Process
#### Export Powerpoint as Video and Pictures
1. Go through and look at the powerpoint for the day you're editing with the script to get an understanding of how things will fit together. If you want to add/update/change things, do that, too
2. To export the slides as videos:  
  1. Go to file -> export. 
  2. In the dialogue box, go to the folder where the videos should be exported (2023-TACoS-Files > Recordings > ENG > day-# > slide-videos)  
  3. Change file format to `mp4`  
  4. Check settings: video compression type = `H.264`, Quality = `Presentation Quality`, dimensions = `1920x1080`, Timing box should be checked, seconds spent on each slide = `5`  
  5. Click `export`
3. To export slides as photos:  
  1. Go to file -> export. 
  2. In the dialogue box, go to the folder where the videos should be exported (2023-TACoS-Files > Recordings > ENG > day-# > slide-videos)  
  3. Change file format to `mp4`  
  4. Check settings: save every slide, 1920x1080  
  5. Click `export`, it will create a folder within the slide-videos folder with an image of each slide  

#### Create and Set Up Project in DaVinci
1. Create your project  
  1. Open DaVinci  
  2. Create/open your project folder (TACoS-CS or TACoS-ENG)  
  3. In that folder, create a new project and name it `#-# TACoS ENG Day # - Topic` (For example, "1-5 TACoS CS Day 1 - Sensors") and click create  
2. Add media  
  1. Go to the media tab by clicking the first icon along the bottom of the screen  
  2. In the top-right panel, locate the TACoS files folder and open the engineering (or CS) folder  
  3. `cmd`/`ctrl` + click to highlight three folders: credits, day#, and music  
  4. Right-click on one of the highlighted folders and select "Add Folder and Subfolders into Media Pool (Create Bins)"  
  5. Select `change` when it asks if you want to change the frame rate  
  6. These folders and their structure should appear in the bottom left panel    
3. Add timeline
  1. Go to the edit tab (the third tab along the bottom)  
  2. On the left-hand side, click on the "master" folder  
  3. In the panel with the folders, right-click and select `new bin`  
  4. Name this bin "TIMELINES"  
  5. Double-click the new bin to open it  
  6. Right-click in the empty space, then select timeslines > create new timeline  
  7. Name this timeline `#-# TACoS ENG Day # - Topic v1` (For example, "1-5 TACoS CS Day 1 - Sensors v1")  
4. Add presenter to timeline   
  1. Navigate to the appropriate presenter video in the bins in the left-hand panel  
  2. Double click the video so that it appears in the media preview panel  
  3. Find the spot right before Alicia starts talking (you can use the right and left arrow keys or `j` + `k`/`l` + `k` to move one frame at a time) and hit `i` to add an in point  
  4. Click and drag the entire video preview to the panel to the right to overwrite it into the timeline
  5. You should have a new video and audio track added to the timeline  
  6. Right-click the audio track head and change track type to > mono
  7. Right-click on the audio in the track and select "normalize audio levels" in the menu. Leave the settings (sample peak program, and -9 dBFS) This will bring the peaks down to the top of the yellow level in the mixer, which is right where we want it. Open the mixer by clicking "mixer" in the top right-hand corner, and check that it looks right
5. Add music to timeline  
  1. Warning: these music tracks are mixed pretty hot. You might want to turn your volume down... Also use `shift` + `s` to turn off audio scrubbing
  2. In the music bin, pick a track that you like. Try to make sure it's different from the previous/next video on that day if you can. Double-click the track to preview it in the top-left panel  
  3. Make sure the playhead in your main timeline is placed at the beginning of the video  
  4. Click and drag the audio preview and hover it over the top-right panel. You will see a menu appear. Drag the preview and drop it on the menu item that says "Place on top"  
  5. You should now have a new audio track below the audio track with Alicia's voice  
  6. If it's not already open, open the mixer (by clicking "mixer" in the top right-hand corner) and adjust the music track levels so that it's topping out around the middle of the yellow (don't worry about the balance with Alicia's voice for now. You can mute Alicia's track by clicking the `M` in the track head)
6. Add slide video to timeline   
  1. Navigate to the slide video for the day/topic you're working on  
  2. Double-click to preview the slide video  
  3. In the preview window, go to the very beginning of the clip. Then, press `+` and type in `20`. Place your in point here (skipping the fade in)  
  4. Make sure your playhead on the main timeline is positioned at the beginning  
  5. Drag the preview clip to the "place on top" menu item  
  6. The slide video should now be in it's own video track, on top of Alicia's video  
  7. Go through the slide video and break out each transition and animation (For each, use the arrow keys to find the beginning of the transition, hit `b` for blade cut mode, click to cut. Then, find the end of the transition and cut there, too. Delete the clips that are not transitions - we will use photos for these static sections because they're much easier to work with)  

#### Edits  
*Note: as you're editing, use audio keyframes to fix any quiet/garbled spots in Alicia's audio, if there are any*
1. Align the beginning of the video so that the screen changes in time with the music. Do this first and make sure you're happy with it before moving on because adjusting it later can be a bit time-consuming    
2. Alicia should start talking on the first colored circle screen. You'll need to set the audio balance here:  
  1. Find a good point in the music a few beats before Alicia starts talking where you'd like to start bringing the music level down (the beginning of the previous bar is good, if you can identify it). Hold `alt` and click on the audio level in the music timeline to create a keyfame  
  2. At the spot where Alicia starts talking, add another keyframe to the music timeline  
  3. Drag the second keyframe down to bring the audio level down. Somewhere around -9 and -11 is usually about right, but listen and use your best judgement, if you get pretty close to a good mix, we can adjust it further while editing. While you're editing, keep an ear out and keep an eye on the mixer. Shoot for around -30/-40ish for music and -10 for Alicia.  
3. Go through the video and place slide transitions. Line up slide changes/animations with keywords. Use the transitions from the slide video and then insert slide images before/after transitions as necessary
4. If there are screen recordings, go through and add them in the appropriate spots (use the "place on top" option). Make sure they are lined up with the Alicia's talking 
5. Now, go through the video and edit everything together. Insert slide photos so that the slide is showing while Alicia is talking about its content. For screen recordings, do your best to keep things in time so that as Alicia says something, it happens on screen. For places in the video where there is no slide/screen recording, or not much is happening, use the video of Alicia to keep things engaging. We'll use a 10s cross dissolve transition when the screen changes. There are two ways to do this. 
  1. If the video clip is on the same track, then use the video transition. Click "transitions" in the top left-hand corner to open, then go to the video transitions folder to find it. Click and drag it onto the spot where the transition should be, and then drag the edge so that it's 10s.  
  2. If the videos are stacked, click the white pentagon in the top corner of the top video and drag it to create a 10s fade. Make sure the video you're transitioning to is dragged back beyond where this fade starts
6. Once you're happy with all of your edits, add the credits at the end. Open the credits bin, and double click the credits video to put it in the preview window. Place an in point at 0.20 (past the fade in... go to the beginning, then `+`, type "20" and hit enter. `i` to add in point). Then, drag it the the right panel menu and drop it over "append at end"  
7. Use keyframes to adjust the music so that after Alicia is finished talking, the audio level rises back to where it was at the beginning of the video. (Add one keyframe with `alt` + `click` where you'd like it to start getting louder, add a keyframe where you'd like it to finish getting louder. Drag the second keyframe up to 0 dB)  
8. Find an appropriate place in the music for it to fade out and cut both the music and the credits at this spot (make sure the credits show long enough that you can read everything on the screen). Then, drag the white pentagon at the end of the music clip so that it slowly fades out (long fades work well)  

#### Check and export
1. Use `cmd`/`ctrl` + `f` for a full-screen preview of your edited video (`esc` to exit). Watch through and check to make sure everything is in order. Make sure audio is good and balanced, make sure you haven't missed any transitions. Make sure the timing is good, and there are no (or at least very few) awkward bits. Remember, done is better than perfect...  
2. When you feel good about it, go to the `deliver` tab at the bottom of the screen  
3. Scroll through the export options to YouTube  
4. Name your exported video `#-# TACoS Section Day # - Topic` for example, "1-5 TACoS CS Day 1 - Sensors"  
5. Click browse to set the file save location to the appropriate Final Videos folder in the TACoS files folder (or, export it wherever is convenient for you and make sure you move it to that folder later)  
6. Leave the rest of the settings the same  
7. Click `add to render queue`  
8. In the top right panel, click `render all`  
9. After rendering, open the exported video and check it  

#### Save DaVinci project to Projects Folder  
1. Go to the DaVinci home screen by clicking the home button in the bottom left corner  
2. Export the project file by right-clicking on the project and choosing "export project"  
3. Navigate to the appropriate folder in the TACoS files folder (TACoS Files > Davinci projects)  
4. Double check that it's named correctly  
5. Click "save"  

### SW Tutorial Notes
  ### Part 1: Bins, organization, rough cuts, and basic clip editing
  - Beginning is about file organization. I'll be importing these files as bins (yes, change frame rate):
    - Day # recording folder (with presenter videos, screen-record videos, and slide videos)
    - Credits folder
    - Music folder
  - Skip ahead to 17:30. (The bit about metadata and smart bins is good info to check out later, but we don't need it for now)
  - Create timelines bin
    - Create new timeline in the bin (ex. "TACoS CS Day 1 - Overview v1") *Note: you probably won't need more than one version of the timeline, but we'll do this just in case we need to make additional edits that we might need to revert*
  - For editing clips:
    - There are different options for adding clips to the timeline. Do whatever works for you. Watch a bit of the video to understand the different options. Can do 
      - Regular drag and drop into timeline (limited usability, but easy)
      - Drag over to the clip viewer (left screen)
        - Setting in and out points in the clip viewer:
          - While viewing clip in source viewer screen (on the left) can use `i` to set in point, and `o` to set out point. 
          - Then use `shift` + `i` to jump to in point, or `o` for out point. `option` + `/` to preview
          - Can set in point, then hit `+` to specify how many seconds of a clip you'd like to jump forward, then hit `o` to set the out point
        - Drag from the clip viewer (can drag the video preview, or just audio/video with the icons at the bottom of the preview window) to the right screen to add to timeline
          - Insert: inserts between clips
          - Overwrite: overwrites starting at playhead
          - Replace: replaces from cursor in viewer screen starting at playhead
          - Fit to fill
          - Place on top: adds new audio/video line and places it at the beginning
          - Append at end: adds at end of timeline
          - Ripple overwrite
    - Detail zoom and `right` and `left` arrow keys to step by fame. `j` and `l` keys are also useful. Can also hold down `k` key and tap J or L to step through frame by frame
  - Working with the timeline
    - Timeline editing modes:
      - Default selection mode (mouse pointer) - `a`
      - Blade edit mode (razor blade icon) - `b`
      - Trim edit mode (block on timeline) - `t`
        - ripple edits rest of timeline
        - can use `.` and `,` to add and remove frames
        - if you click on upper part of clip, you can do a slip edit
    - Zoom options
      - Full extent zoom
      - Detail zoom
      - Custom zoom
        - `cmd` + `=` to zoom in
        - `cmd` + `-` to zoom out
        - `shift` + `scroll` to dynamically change (with mouse)
        - timeline view options button can also be used to adjust height of tracks
    - Navigation
      - Use `right` and `left` arrow keys to step by fame
      - Use `j` and `l` keys to play forward and backwards. Can also hold down `k` key and tap `j` or `l` to step through frame by frame
    - Linked clips: audio/video automatically linked so you can't accidentally un-sync them. It's very important to keep things synced up!
      - `option` or `alt` key to temporarily ignore the link
    - Locking layers works just like other editors
  - Adding and working with audio
    - double click to preview (top waveform is overview, bottom two are zoomed in (stereo))
    - remember `shift` + `s` to turn off audio scrubbing
  - Adjust speed of shots
    - Select clip
    - Go to inspector (top right-hand part of the screen)
    - Scroll to find "speed change"
  - Match frame and replace
    - `f` to view source of clip in source viewer 
    - move playhead in source viewer screen to where you'd like the clip to start
    - drag over to replace over edit, `f11` or, click the replace button above the timeline
    - other cool things to do with this, at about 1:06:00 but we don't need them for TACoS

  ### Part 2
  - He suggests duplicating timeline to have a backup
    - Timelines bin, right click timeline, duplicate and rename (v2)
  - Add audio tracks
    - Right click the timeline track heads and click "insert tracks" 
  - Rename tracks by just clicking on them. Can color code if you want
  - He talks about adding voiceover for a while, skip that part
  - 8:50 starts talking about SFX. Can skip this, too, since we won't use them
    - select a clip and push `x` to mark in and out based on the length of that clip, then overwrite edit directly into timeline
    - back-timed edit (use when you know where you want the clip to end)
    - use replace edit to sync up audio SFX/video
  - Audio: this stuff is really important! (16:50)
    - Click mixer button in top right hand corner, and expand it out (can hide the media pool to get a little more space, just toggle media pool with the button in the top left)
    - Remember, you want audio levels (for the voice) to be around the yellow area of the meter
    - Learn about mono vs stereo
    - If you need to reconfigure audio: right click -> clip attributes -> audio
    - Audio levels
      - Rule of thumb -12 dBFS is a good output mix. You want the voice clips to be peaking just at the top of the yellow bar in the mixer
      - Remember to focus first on consistency, you can adjust full track levels and balance between tracks later
      - Balancing audio:
        - Add keyframes: hold `option` or `alt` and click on volume curve, then click and drag audio curve to change volume
        - Can use `option` + `cmd` + `-` to nudge audio level down one dB at a time
        - Can also normalize audio:
          - Sample peak program is the simplest
          - -9 dBFS level = set upper part of peaks (so that'll be the top of the yellow bar in the mixer)
          - independent so it sets each clip on its own
        - Notice that he mixed the SFX lower than the voice, but focused mostly on consistency between clips on each track
      - Audio fade is easy! Just have to click and drag the white pentagon at the beginning/end of clip
        - Can do crossfade by placing one layer over the other, with one going in and the other going out
        - He says that these audio fades are more accurate than audio transitions (which is another option)
    - 31:45 he talks about music. We will use music for TACoS
      - 33:30 - this bit is important. He's talking about increasing the audio level during the credits. For TACoS, we want the music to be louder at the beginning and during the credits
      - Add a couple keyframes at the beginning, after the intro, and add a couple keyframes at the end, going into the credits. 
  - 34:00 Transitions
    - Hah, he says to use transitions sparingly (I never knew that!) to indicate change of time or location. 
    - Select edit point, go to menu > transition
    - Can change duration by clicking and dragging
    - Change style of transition
      - Select transition, then click inspector in the top right
      - Can change transition type (generally, I just use cross desolve for TACoS, but you can be creative if you want)
        - more transitions in the effects library > toolbox > video transitions
  - 37:25 Titles - good info but we don't need them for TACoS, since we made them in PowerPoint
  - Workspace > viewer mode > cinema viewer (playback fullscreen, also `cmd` + `F`) `escape` to return

  ### Other Useful Tricks
  - Add freeze frame (to re-time powerpoint slides)
    - Place playback point at the frame you'd like to freeze on
    - Right click on the clip and select "retime controls" (or `cmd` + `r`)
    - Click black arrow dropdown that appears on the clip and choose "freeze frame"

### Keyboard shortcuts
- `shift` + `delete` = ripple delete
- `/` to play through
- `shift` + `cmd` + `,` or `.` to shuffle clips backwards and forwards in the timeline
- Position playhead + `alt` + `y` = select clip and everything to the right
- `cmd` + `r` = time settings on a clip
- `shift` + `s` = toggle audio scrubbing
- `n` = toggle snapping
- `home` = jump to beginning
- `up arrow` = jump to previous edit point
- `down arrow` = jump to next edit point


