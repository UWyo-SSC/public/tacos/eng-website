# 2023 TACoS: Engineering

This repo contains all project materials for the 2023 TACoS engineering activities.

## Website

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

## Getting started  
- Install the [latest version](https://nodejs.org/en/) of nodejs (18.14.2)  
- Install yarn: `yarn install`  
- cd into the website folder  
- Run `yarn upgrade @docusaurus/core@latest @docusaurus/preset-classic@latest`  
- Run `yarn start`  
- If node errors appear, follow steps for enabling legacy OpenSSL provider  
  - On Unix-like (Linux, macOS, Git bash, etc.): `export NODE_OPTIONS=--openssl-legacy-provider`  
  - On Windows command prompt: `set NODE_OPTIONS=--openssl-legacy-provider`  
  - On PowerShell: `$env:NODE_OPTIONS = "--openssl-legacy-provider"`  

## When editing website locally  
- The website is edited via markdown files in the docs (for text) and static (for images/videos) folders  
- To view your changes locally (following initial setup):  
  - cd into `cs-website` folder  
  - `yarn start`  


## More Docusaurus info:  
### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

```
$ GIT_USER=<Your GitHub username> USE_SSH=true yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.
