---
id: 'day5'
title: 'Day 5: Bringing it All Together'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/cyMBqcP7ljk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Welcome to day 5 of The Artful Craft of Science - Engineering! Today we are going to put our new knowledge to the test. Remember that spaghetti tower you built at the beginning of the week? We're going to do that again, and see how much better we can do now that we understand the engineering design process! 

## Final Activity: Spaghetti tower, but way better!

<iframe width="560" height="315" src="https://www.youtube.com/embed/dX_Qlo_Iu5s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

We're going to use everything we learned this week to build a new and improved spaghetti tower, just like we did on Day 1. Instead of winging it, though, today we'll follow the engineering design process. We want to leave as much time as possible to plan, build, test, and reiterate, so let's dive right in!

### Setup

Find a clean, flat area where you can build and test your tower. Then, gather your materials: 
- 20 Dry spaghetti noodles, broken in half
- 1 Big marshmallow
- 16 Mini marshmallows
- A handful of rubber bands
- A ruler

### Step One - Engineering Design Process Review

While you're planning, building, testing, and reiterating on your spaghetti tower design, you can use the steps of the engineering design process to help you stay organized and be productive. Before you get started, let's look briefly at how you can use each step to help you build the best spaghetti tower:

1. Understand the problem:  
  i. What problems does our spaghetti tower solve?  
  ii. What problems do we need to solve to build a strong spaghetti tower?  

2. Complete background research:  
  i. What has worked in the past?  
  ii. What hasn't worked?  

3. Imagine and evaluate possible solutions:  
  i. Before you build, come up with a few different ideas. Maybe you'll discover something crazy enough that it just might work.  
  ii. Then, pick the best idea and go for it!  

4. Develop and prototype a solution:  
  i. You know what to do!  

5. Test solution:  
  i. Can your tower support the marshmallow?  
  ii. Could it support even more marshmallows?  

6. Assess prototype and reiterate:  
  i. How can you make your tower better?  
  
7. Communicate results:  
  i. Make sure you tell your friends and family all about how cool your spaghetti tower is.  

Remember, the engineering design process is continuous and iterative. You don't have to follow all of the steps, and you don't have to do the steps in any particular order. Instead, think of these steps as different paths you could take to improve your design. 

### Step Two - Do the thing!

You can spend the rest of our time today working on your spaghetti tower. When there's about 15 minutes left, come back for step 3.

### Step Three - Final Marshmallow Test
Welcome back! Hopefully, you feel pretty confident in the spaghetti tower you've built using all the new skills you learned this week because it's time for the final marshmallow test. Go ahead, hold your breath, and do the honors!

### Step Four - Reflection
How did it go? Was your Day 5 spaghetti tower better than your Day 1 spaghetti tower? If your tower fell, remember, it's all part of the process.

Even though we're out of time for today, you could continue to iterate through the design process until you have the ultimate spaghetti tower. You could build a whole city of buildings, each one better than the last. You could make other spaghetti buildings, designed to do different things: apartments, parking garages, theaters, art galleries, castles... you name it! 

When you're finished building your spaghetti empire, you could use your new skills to solve some other engineering problems, and make life better for you and the people around you. 

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/peZuGfd47bE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Thank you for joining us on this engineering adventure! Hopefully, you had fun building the best spaghetti tower and the best micro:pet home - we bet you came up with some pretty neat ideas! Don't forget to show off your projects to your friends and family, and remember, the possibilities for what you can create are endless. Don't stop here. Keep exploring and trying things, who knows what you'll make next? 
