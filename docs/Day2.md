---
id: 'day2'
title: 'Day 2: Plan and Build'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/zWdE-R0t85M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to day 2 of The Artful Craft of Science - Engineering! Today we are going to go over the first four steps of the engineering design process:

- Step 1: Understand the problem
- Step 2: Complete background research
- Step 3: Imagine and evaluate possible solutions
- Step 4: Develop and prototype a solution

As we learn about these steps, we will apply our new knowledge to design our own solution to a noisy problem!

## 1: Understand the Problem

<iframe width="560" height="315" src="https://www.youtube.com/embed/JlQ0n2EVRIE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Yesterday, we talked about what engineers do: they design solutions to solve problems. So, the first step to an engineering project is to find a problem and carefully define the needs and constraints involved. 

### Find a Problem to Solve

Take a moment to think about some problems in the world that could be solved with a bit of clever thinking. These can be small problems, like:

* It's difficult to get the last drop of milk out of the carton
* People often missplace items (like the TV remote, your phone, or keys)

Or they could be bigger problems. For example: 

* Headlights from oncoming cars make it difficult for drivers to see at night
* There are people who don't have access to safe drinking water

We can use engineering to solve many different problems! This week, we are going to use our new knowledge to solve a noise problem.

In the computer science section of TACoS, we will be building a Micro:Pet. The problem is: **these little guys are very sensitive to noise, and they need a quiet place to sleep at night**.

<!-- Loud environments are distracting and potentially even harmful.  -->

Let's get started!

### Define the problem

Once you have decided on a problem you'd like to solve, it's time to define the problem. We need to know the **who**, **what**, and **why**. Who does this problem impact? What exactly is the problem? Why is it important to solve this issue? We'll use these questions to write a **problem statement**, which is a sentence that declares, "*Who* needs *what* because *why*." 

Let's further define our problem: 
- **Who** is effected? Our micro:pets.
<!-- **Who:** is bothered by noisy environments when they're trying to get work done? Lots of people are, almost everyone. We should be a bit more specific. Let's focus on someone that we're all pretty familiar with: *kids who are trying to do homework*.  -->
- **What** do they need? A home with excellent sound-proofing!
<!-- - **What** do these kids who are trying to do homework need? Well, if loud environments are the problem, then they need a way to cut down on the noise. Let's focus on a specific type of noise that we want to cut down on: *too many notifications from phones, computers, and tablets*. -->
- **Why** would sound-proofing be important? Loud noises wake micro:pets up, and can scare them. 
<!-- - **Why** would cutting down on notifications be helpful? They're distracting! It makes sense that providing a quiet environment would help students stay focused.  -->

With all that in mind, here's our problem statement: 

**Micro:pets need excellent sound-proofed homes to keep them safe and happy.**
<!-- **Kids who are trying to do homework need a way to reduce distracting notifications from phones, computers, and tablets because it will help them stay focused and finish their homework faster.** -->

### Specify the requirements

Once we have a clear understanding of the problem we want to solve, we can specifiy requirements for a good solution. We can apply the same thinking now to identify the requirements for our micro:pet homes:
1. It needs to reduce noise.
2. Just like in the spaghetti tower activity, you'll have limited time and materials.
3. Whatever solution you design must **fit inside a small cardboard box**, and you only get one box to work with.
4. Since we only have one box, your designs need to be crafted in such a way that they can be removed and updated without destroying the box.
5. Additionally, the sensor inside the box will need to be plugged in to a computer, so we'll need a way for that cable to get through the box.

We may need to add more requirements to this list later, but this is a good start!

<!-- CUT
For example, think back to the spaghetti tower we built yesterday. A successful tower had to be: 

- as tall as possible
- strong enough to support the weight of a marshmallow on top
- built out of a limited supplies
- easy to assemble quickly within the time limit

Note that some of these requirements have to do with the goal (building a strong and tall tower), and some of them have to do with inherent limitations.  -->

## 2: Complete background research

<iframe width="560" height="315" src="https://www.youtube.com/embed/6R9tS-Fbmd8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The second step in the engineering design process is to complete background research. Take a look at the full picture by exploring the problem from different angles. In the previous step, we identified the **who**, **what**, and **why** of our problem. A good place to start your research would be to learn more about each of these elements.

### Ask Questions
- Explore the who by asking questions about the person who will use your solution.
- Investigate the what by taking a deeper look at the problem and possible solutions.
- Research the why by learning more about why this solution is important.

<!-- #### Explore the Who -->
<!-- Ask questions about the person who will use your solution. -->
<!-- * Who has this problem?  -->
<!-- * What is their experience with the problem?  -->
<!-- * Who would be willing to buy the product and for how much money?  -->

<!-- #### Explore the What -->
<!-- Take a deeper look at the problem and possible solutions. -->
<!-- * What solutions already exist to this problem or similar problems?  -->
<!-- * How do these similar solutions work, and what are their drawbacks?  -->
<!-- * What mistakes have been made before when trying to solve this problem? -->

<!-- #### Explore the Why -->
<!-- Learn more about why this solution is important. -->
<!-- * What makes this problem significant?  -->
<!-- * What will solving this problem accomplish? -->

### Find Answers
Use library resources and the internet to help you on your quest. We'll practice this in just a moment. You can also use **networking**, which is talking to people who have experience in the area you are researching. Think about knowledgeable adults in your life, maybe a teacher, or a family member, who would have some valuable insight.

<!-- Another great resource you can use is **networking**, which is talking to people who have experience in the area you are researching. Think about knowledgeable adults in your life, maybe a teacher, or a family member, who would have some valuable insight. Start a conversation by asking these people specific questions about your topic. -->

## Mini Activity: Search engine challenge

<iframe width="560" height="315" src="https://www.youtube.com/embed/18DVX_6VXwI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that we've defined our problem and understand it a bit better, we're ready to do some background research! Normally, engineers would be thorough in their research, but we have limited time. So, we're going to put your search engine skills to the test! You will have five minutes to find the answers to as many of our questions as possible.

### Step One - Set the timer
Set a five-minute timer and prepare yourself. You might want to get out a piece of paper or open a text document to write down your answers so you can keep track of how many challenges you manage to complete!

### Step Two - Answer as many as possible!

<details>
  <summary>Search Challenge #1</summary>

> What sort of materials are best at *reflecting* sound?

</details>

<details>
  <summary>Search Challenge #2</summary>

> What sort of materials are best at *absorbing* sound? 

</details>

<details>
  <summary>Search Challenge #3</summary>

> What is it called when two parts of a wall are separated to prevent sound from travelling through? 

</details>

<details>
  <summary>Search Challenge #4</summary>

> What is the difference between amplitude and frequency?

</details>

<details>
  <summary>Search Challenge #5</summary>

> What is the logarithmic unit used to measure the power or intensity of sound? 

</details>

<details>
  <summary>Search Challenge #6</summary>

> At what level do sounds start causing hearing damage (in dB)?

</details>

<details>
  <summary>Search Challenge #7</summary>

> What would it sound like if a noise was attenuated by 10 dB? (*Hint: you'll need to know what "attenuate" means, and also determine how our ears preceive a change of 10 dB*)

</details>

<details>
  <summary>Search Challenge #8</summary>

> What is the range of human hearing? 

</details>

<details>
  <summary>Search Challenge #9</summary>

> You're staying in a hotel room with your family. Late at night, when it's quiet and dark, you notice an extremely irritating, high-pitched whine coming from somewhere in the room. A mosquito! It's driving you crazy, but your older family members hardly even notice! It's so high pitched and loud; why can't they hear it? 

</details>

<details>
  <summary>Search Challenge #10</summary>

> List as many places as you can where soundproofing is used

</details>

<!-- 1. What sort of materials are best at *reflecting* sound?
2. What sort of materials are best at *absorbing* sound? 
3. What is it called when two parts of a wall are separated to prevent sound from travelling through? 
4. What is the difference between amplitude and frequency?
5. What is the logarithmic unit used to measure the power or intensity of sound? 
6. At what level do sounds start causing hearing loss (in dB)?
7. What would it sound like if a noise was attenuated by 10 dB? (*Hint: you'll need to know what "attenuate" means, and also determine how we preceive a change in 10 dB*)
8. What is the range of human hearing? 
9. You're staying in a hotel room with your family. Late at night, when it's quiet and dark, you notice an extremely irritating, high-pitched whine coming from somewhere in the room. A mosquito! It's driving you crazy, but your older family members hardly even notice! It's so high pitched and loud; why can't they hear it? 
10. List as many places as you can where soundproofing is used -->

### Step Three - Check back in

How did it go? Do you feel like a search engine ninja? If you want to, you can take a moment to check your answers below. This background information will be helpful later!

<details>
  <summary>Check your answers here!</summary>

> 1. Hard materials like concrete, masonry, and metals.
2. Soft fibrous and porous materials like cloth, foam, and cork.
3. Decoupling
4. Amplitude determines how loud a sound is, frequency determines pitch, or how high or low it sounds 
5. Decibels
6. Over 85 dB
7. It would be quieter and sound about half as loud
8. 20 Hz - 20 kHz
9. As people get older, our ability to hear high frequencies decreases over time
10. Concert halls, art galleries, residential buildings, offices, schools, cars...

</details>

## 3: Imagine and evaluate possible solutions

<iframe width="560" height="315" src="https://www.youtube.com/embed/gTghl6D6lnc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that we understand our problem, and we have some background knowledge, let's talk about brainstorming and evaluating possible solutions. 

### Brainstorming
Brainstorming is the process of listing out as many ideas as you can think of. When you're brainstorming, it can be very tempting to go with the first idea that comes to mind, especially if you're feeling stuck. However, the first idea we have may not be the best solution. In fact, it might not even work at all! We want to make sure that we spend our time and resources on good ideas, so it's important to explore many different solutions.

<!-- While brainstorming, don't worry about which ideas are good and bad. The point is to get the ball rolling on creative designs. Be open-minded when you're brainstorming because it might lead to a stroke of genius. Also, make sure you write down all of the ideas you come up with. Use both words and pictures to help make your thoughts more complete. -->

<!-- There are lots of ways to get the ideas flowing. For example:

* Think about existing solutions and how they can be used as a starting point
* Gather a group of 3-5 people and talk it out
* Think about what you're trying to design and compare it to something completely different (e.g. how is designing a noise-reducing box similar to designing a train station?)
* Take a break -->

To save some time, we brainstormed a few possible solutions for our micro:pet homes. We asked ourselves: 

"What can we build quickly, with limited materials, that fits inside a small cardboard box and reduces external noise?"

Here's what we came up with:
* Program a noise cancelling device for inside the home
* Cover the interior with sound-proofing materials
* Relocate the home
<!-- * Program a signal-stopping device, and whenever we push a button, all of the electronics in a 10ft sphere are automatically destroyed. No more notifications! Ever! -->
<!-- * Put the noisy devices *inside* the cardboard box, and then pad the box to provide some soundproofing. -->
<!-- * Cut the box in half and create a very fashionable pair of noise-cancelling headphones, or maybe some noise-reducing earmuffs. -->

### Evaluating and selecting solutions
Now that we've got some ideas down, it's time to decide which solution is the most promising. There are three main criteria we can use to compare the possible solutions: defined requirements, nice to haves, and universal design criteria.

1. **Defined requirements:** These are the requirements that we defined in the first step of the engineering process. How effective is it at doing its job?

2. **Nice to Haves:** These are items that are not required, but would be nice to have. For example, it would be nice if our box looked really cool! 

3. **Universal design criteria:** These are common characteristics that all good designs have. For example, good designs are safe to use and build, equitable and accommodating (anyone can use it), and free of unnecessary complexity. 

Compare the proposed solutions and rank them based these criteria. Using tools like a pros and cons list, or a decision matrix can be helpful here. Looking at our options, we have a clear winner. We should add sound-proofing materials to the inside of the home!

<!-- Let's evaluate our noise-reducing ideas: -->
<!-- * Signal-stopping device: This idea is a bit too permanent, probably evil, and won't be possible to do with the materials we have. Besides, we're a bit worried about the potential health issues involved... What if someone nearby is wearing a medical device?  -->
<!-- * Soundproof box: This solution seems doable. It checks all the boxes, no pun intended, and is free of unnecessary complexity. -->
<!-- * Noise-cancelling headphones: This could work, too, but we'd need to do more research. Also, headphones are probably beyond our means, and earmuffs are out of style. -->

<!-- Looking at these options, we have a clear winner. We should add sound-proofing materials to the inside of the home! -->

<!-- ![white box](/pictures/day2/box.jpg) -->

## 4. Develop and prototype a solution

<iframe width="560" height="315" src="https://www.youtube.com/embed/aVn_IjvwpcI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that we have a good idea, we will **develop** the solution by using tools like drawings, prototypes, and models to plan and enhance its design. Good engineers will often **iterate** over their designs, improving and adding features. Development is ongoing because the more you dive into your project, the more you will learn about it, and the better you'll be able to make it! Let's try it out!

## Activity: The Micro:Pet Home - First prototype

<iframe width="560" height="315" src="https://www.youtube.com/embed/L98aBVnh3QU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

It's finally time to get our hands dirty with our first Micro:Pet Home prototype. 
<!-- *(We've decided to call it The Focus Box, but if you've got a better name for it, that's cool, too.)*  -->

<!-- At this point, we've got a **problem statement**: *Kids who are trying to do homework need a way to reduce distracting notifications from phones, computers, and tablets because it will help them stay focused and finish their homework faster.* -->

<!-- We know the **requirements**:  -->
<!-- * The box must reduce noise. -->
<!-- * We have limited supplies (one box). -->
<!-- * The design must be easy to assemble in 15-20 minutes. -->
<!-- * It needs to fit inside a small cardboard box. -->
<!-- * The soundproofing must be easily removable and adjusted. -->
<!-- * There needs to be a way to connect a micro:bit that's inside the box to a computer outside of the box. -->

<!-- We learned some things about soundproofing, did a bit of brainstorming, and are ready to build a prototype!  -->

### Setup
For this project, you can use:
- The box that your TACoS materials were provided in
- 50 cotton balls
- 2 feet of bubble wrap
- 2 felt sheets
- 2 foam sheets
- A handful of rubber bands

You should also grab:
- Scissors
- A roll of tape
- A ruler
- A writing utensil

You're also welcome to explore other materials throughout the week. You might consider things like another box, packing peanuts, cork board, tin foil, etc... Doing some extra background research might help you get some good ideas!

Collect these items and make sure you also have a flat surface to build on.  

### Step One - Plan

Before you dive into your first micro:pet home prototype, it's a good idea to make a plan. Think about what you discovered during the search engine challenge. Specifically, we learned that there are different methods used for soundproofing and noise-reduction: 

- Hard materials **reflect** sound
- Soft/porous materials **absorb** sound  
- Putting a room within a room **decouples** sound

Think about the materials you have in front of you and use these principles to create your first prototype. The real magic comes from combining the three methods above in creative ways. You can cut, stuff, stick, and bend materials to make your design work, but while you're planning, keep in mind that you'll need reuse these materials when you build your second and third prototypes. So, you might want to be extra sure before you decide to do anything too drastic!

Take some time to draw out your idea. Once you've got it down on paper, move on to step two!

### Step Two - Build

It's finally time. You've thought a lot about your design, and you've got schematics in hand for how you'll build it. Let's make something awesome! You've got the rest of the hour to build your first prototype. 

Tomorrow, we'll test things out and see how you did. Good luck!

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/QGsbujDrh_w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Today, we learned the basics for following first four steps of the engineering design process:

- Step 1: Understand the problem
- Step 2: Complete background research
- Step 3: Imagine and evaluate possible solutions
- Step 4: Develop and prototype a solution

As we learned the steps, we applied our new knowledge to build the first prototype of a micro:pet home! We only scraped the surface of the engineering process, so if you're interested in learning more about these steps, be sure to check out [the Science Buddies website](https://www.sciencebuddies.org/science-fair-projects/engineering-design-process/engineering-design-process-steps). Tomorrow, we'll test our designs to see how well they work, and we'll make them even better! 
