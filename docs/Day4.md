---
id: 'day4'
title: 'Day 4: Communicate Results'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/G4vEq4lu6zo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to day 4 of The Artful Craft of Science - Engineering! Today we are going go over the final step of the engineering design process:

- Step 7: Communicate Results

We'll learn how to document both our solutions and the process we took to build and refine them. 

Finally, iteration is extremely important in the engineering process, so we'll also take one more shot at improving our micropet homes, with a twist. 

## Communicate Results

<iframe width="560" height="315" src="https://www.youtube.com/embed/DYx35x4Y1jM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Engineers must communicate with one another as well as with management, accounting, production, customers, and other departments. This requires great communication skills, and there are many ways to communicate results. Professionals often communicate their results by writing final reports. They might also share a poster or give a presentation, like we do during our CEDAR Symposium each fall! 

There are many opportunities for students to share their results, too! Ask your teachers about how you might be able to share your work at a science fair or other competition. Let's go over what we include in a final report and practice by thinking about what we would might write for our pet home project! 

- Final Report:
  - Title Page - The title gives readers a clear picture of what your report is about. If you were writing a report about your micro:pet home, what would you title it?
  - Abstract - An abstract is an abbreviated version of your final report. If you had to explain everything you did and discovered in 200-400 words, what would you say?
  - Question, Variables, & Hypothesis - This section is used to explain the problem you were trying to solve, what variables you tested or changed, and your hypothesis. It would include the problem statement we wrote, an explanation of your testing methods, and what you predicted your results would be
  - Background Research - This is the research paper you wrote before you started your experiment. It would include everything you discovered during the Search Engine Challenge.
  - Materials - This section should list everything you used to make your project. Can you remember everything you used in our activities so far?
  - Data Analysis & Discussion - This section is a summary of what you found out in your experiment, focusing on your observations, data table, and graph(s), which should be included at this location in the report. What noise levels did you observe when testing each of your prototypes? What's the best way to convey that data?
  - Conclusions - Here you can include ideas for future research, and you can also add advice you would give to someone else attempting to replicate or continue your work. What advice would you give to someone trying out the activities you've done so far?

## Activity: Building on a Budget

<iframe width="560" height="315" src="https://www.youtube.com/embed/Zfr0fFs1hpI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

By now you've become quite the engineer and you've built multiple versions of your tower and your micropet home. Today, we're going to build our best home yet! But there's a catch...

In the real world, engineers must work within the bounds of their budget. Now, you will do the same while you create the final version of your micro:pet's home. That means, no more free stuff! 

For this activity, you'll have the same materials as usual, but now they'll cost imaginary money to use, and you've only got $200 imaginary dollars to spend. 

### Setup
For this activity, you will need these items for testing:
- Your micro:pet home (second prototype)
- A computer, with Chrome installed
- Noise logger (Sensor:bit + micro:bit + noise sensor)
- A micro:bit cable
- [Noise logger code](https://makecode.microbit.org/v4/_FMAdeoWhqavm)

For building your third prototype, you will need: 
- The plans for your second Micro:pet home prototype
- The box that your TACoS materials were provided in
- 50 cotton balls
- 2 feet of bubble wrap
- 2 felt sheets
- 2 foam sheets
- A handful of rubber bands
- Scissors
- A roll of tape
- A ruler
- A writing utensil

### Step One - Test Your Second Prototype
Let's take a moment to test our second prototype and see how it performs! We'll do the [same thing we did yesterday](https://community.pages.ssc.dev/wycs/tacos/2023/eng-website/docs/day3#activity-test-the-micropet-home) to test our first prototype. Try to keep your tests consistent by doing everything the same way as yesterday. Use the same noise-making device, the same distances, the same volumes, etc., and keep everything as consistent as possible. Here's a quick summary of what you need to do:

1. Assemble your hardware. Your micro:bit should be inserted into the sensor:bit, and the noise sensor should be plugged in to pin 1.
2. Connect your micro:bit to Chrome and flash the [Noise logger code](https://makecode.microbit.org/v4/_FMAdeoWhqavm) code onto it.
3. Click on "show console device" and make sure the graph is working.
4. Chart your two "no noise" groups. Make sure to save or write down these "control" measurements so you can analyze them later.
5. Conduct the same tests that you did yesterday, by adding additional noise. Make sure to save or write down all of your testing data. 

### Step Two - Assess
Just like we did yesterday, we need to assess our test results. Remember to first address any big problems, and then look at areas that can be improved. 

#### Step 2a: Address Problems
Think about obvious problems. Did your improvements solve any big issues you found yesterday? Were there any new problems in today's testing? 

#### Step 2b: Improve the Design
Next, take some time to compare testing data from the first and second prototypes. This is where the control measurements come in handy! 

1. Start by comparing what noise levels you got while testing the micro:pet home in a quiet environment with no additional sounds. Were they different? If the base level of noise was higher or lower than yesterday, keep that in mind while you're comparing other data. 
2. Then, compare the data from the tests you did while adding noise. Did your second prototype perform better?  

Again, think of one thing that went well, and one thing that didn't go well, and use that to decide on how you can make further improvements.

### Step Three - Understand the Budget
At this point, you should have an idea of what you'd like to change to improve your micro:pet home even further. Here's where that extra twist comes in: the budget. 

Before we make any decisions about updates, let's look at our new budget. Take a moment to list all of the materials you used in your second prototype, and use this table to figure out the total cost for those items.

| Material                    | Price          |
|-----------------------------|----------------|
| 1 Cotton Ball               | $1             |
| 1 foot of bubble wrap       | $20            |
| 0.5 Felt Sheet              | $25            |
| 0.5 Foam Sheet              | $15            |
| 1 inch of tape              | $1             |
| 1 rubber band               | $1             |

For example, let's say that I used 50 cotton balls, 6 foam sheets, 4 felt sheets, and around 10 inches of tape to build my second prototype. That would add up to: 

50 cotton balls = $50
6 foam sheets = $90
4 felt sheets = $100
10 inches of tape = $10
TOTAL = $250

That's a lot of money! Here's where things get tricky... Now, we only have $200 imaginary dollars to spend -- $50 less than what we used in our example box! While you're planning your third prototype, not only will you need to find a way to make further improvements, but somehow you'll have to do it with fewer materials. Let's make a plan!

#### Step Four - Plan 

The next step is to take all of the information we have and make a plan for the third prototype. Consider the big issues that need solved and the improvements you'd like to make and try to figure out how you can balance those things with the additional limitation from your new budget. Ask questions like: 
- What materials seem to be working the best? 
- What could be left out? 
- Are there items that could be traded out?

Sometimes, it's difficult to make compromises, especially when you're not totally certain what the best decision will be. Take some time to make those tough calls, and draw out your final prototype plan. For extra bonus points, see if you can finish the day with leftover money. Who knows what imaginary things you can buy with those imaginary dollars...

When you're finished drawing out your final plan, move on to step five! 

### Step Five - Build and Test

Now that you've got a plan and have some materials, all that's left to do is build and test your newest prototype! You have the rest of the day to implement additional improvements and create your final Micro:pet Home. After you're happy with your final design, you should take some time to test it out and see how it compares to your other two prototypes. Good luck! 

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/fHtqvjTjiLw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Today, we learned about the final step in the engineering design process and how to communicate our results in the form of a final report. We also constructed the final iteration of our micropet home while building on a budget. Hopefully, you've got a good home that will make your micro:pet comfortable and happy! If not, maybe consider returning to the drawing board! Even the best homes have room for improvement... Can you imagine additional upgrades that could be made? 

Tomorrow is our last day. We'll use everything we learned this week and put our engineering skills to the test one final time! 
