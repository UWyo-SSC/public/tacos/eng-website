# TACoS Curriculum Proposal
## Updated Lesson Plans
### Day 1 - What is CoSci?
- CoSci: Introduce computer science and computers. 
- Cybersecurity: What you do online isn't private, visibility of stuff 
- Hardware: Introduce micro:bit and makecode editor (sensor?) 
- Biology stuff: Fireflies? 
- Activity: Fireflies?
- Things we give them: some sort of simple code to flash to micro:bit that's interactive - fireflies that are interactive in some way, maybe flying around like the bumblebees will later
- ***Potential separate videos, with section headings***
  - If we use separate videos, we can reuse content from last year, too 

### Day 2 - Variables
- CoSci: Variables 
- Cybersecurity: Client/server, different types of computers that do different things. Server = hive, client = bumblebee?
- Hardware: Game controller 
- Biology: Bumblebee game (potentially push honey pot part to day 3) 
- Activity: Hive/timer/bumblebee whatever works
- Things we give them: Some parts of the game 

### Day 3 - If Statements/Booleans
- CoSci: If statements/booleans 
- Cybersecurity: Problems can arise when computers communicate with each other
- Hardware: Radio groups, communication parts of micro:bits 
- Biology: 
- Activity: Hive/timer/bumblebee whatever works
- Things we give them: 

### Day 4 - Control Flow and Loops
- CoSci: Control flow/loops 
- Cybersecurity: Man-in-the-middle attacks
- Hardware: n/a
- Biology: Spiders
- Activity: Add spider adversary to bumblebee game 
  - Game addition: bee gets stuck in spider webs. If a spider catches your transmission when sending pollen to the hive, you could lose the pollen or we could freeze the bee game. Want to show off how radio groups work
    - Blocks initiate radio to communicate between micro:bits. works in radio groups. If you send data here, another micro:bit can pick up your transmission, modify it, and send it along (man in the middle attack) maybe when you shake off your pollen, the spider has a chance to steal your "transmission" and eat the bee
    - Could say, "here's the adversary" go explore how it works and what's happening to your bee. How do I recognize an attacker? How do I work in an ecosystem with attackers?
- Things we give them: complete bumblebee game to this point. Also possibly spider code "Hey, if you try to send pollen back to the hive, and your pollen amount is >5 (or wherever the bee starts to slow down), the bee might get captured in the web... However you want to work it
- Program day/night cycle, change game so that it follows day/night rules

### Day 5 - Big final activity!
- CoSci: Functions/methods
- Cybersecurity: think like an attacker - why does a bear eat at night? The bees can't protect the hive at night.
- Hardware: n/a
- Biology: Bears eat honey, at dusk :) 
- Things we give them: Example of a way to do it
- Come up with a fun way for how the bear can try to eat all the honey
- Bear game: bears have to somehow find the hive/maybe traverse a maze to steal the honey. 
  - Creative part: whatever things the bear has to do to take the honey 
  - Also this is multiplayer as in one person can be the bees, and one can be the bears
- Give a few different options/examples, but they can go off on their own if they feel like it

# Original Planning
## Fireflies

### Prompt

Swarms of fireflies manage to sync up their flashing without anybody telling them to do so! In this lesson we will teach microbits to do the same thing.

### Objectives

- Understand how fireflies sync up
- Understand how Micro:Bits communicate
- Understand how to put basic programs on Micro:Bits

### Lesson

1. Introduce computer science and the micro:bit
2. Introduce how to put simple programs on the Micro:Bit
3. Send messages between Micro:Bits
4. Show off how Fireflies sync up
5. Present basic Micro:Bit algorithm
6. Program synchronization of Micro:Bits

### Activity: Fireflies on micro:bit

## Bees

### Prompt

Bees take on multiple jobs in the process of making honey. In this lesson, we'll program three different bees: a pollinator bee who gathers nectar; a worker bee who processes the nectar into honey; and a storage bee who takes the honey, puts it in the comb and seals it up with bees wax.

### Objectives

- Understand the process of making honey
- Understand the ways to interface with the Micro:Bits
- Understand how to send more complex information between Micro:Bits

### Lesson

1. Introduce the ways to interact with the Micro:Bits (i.e. buttons, shaking, compass, etc.)
2. Show off how Bees make honey
3. Program a pollinator bee to "go collect pollen"
4. Program a worker bee to "process the honey"
5. Program a storage bee to track how much honey has been made

### Activity: Pollination Game

## Bears

### Prompt

Bears hibernate throughout the winter as a way to conserve energy. In this lesson, we'll learn how to use sensors to understand how close winter is and program a bear to know if it should be hibernating, just drinking water, or scrounging for food.

### Objectives

- Understand the hibernation cycle
- Understand how instruments and sensors connect to Micro:Bits
- Understand control flow and decision making in programs

### Lesson

1. Introduce If...Then programming structures
2. Introduce sensors
3. Show off how Bears hibernate
4. Write a program to take temperature and decide if the bear should be eating, drinking, or sleeping

### Activity: 

## Monarch Butterflies

Introduce swarm behaviors
Extend sensing to plan migration
Extend communication to plan migration

## Orioles

Introduce predators
Have a way to "eat" the monarchs
Challenge students to try to find a way to protect the butterflies
